
<div align="center">
    <a href="https://github.com/zhaofeng092/python_auto_office"> <img src="https://badgen.net/badge/Github/%E7%A8%8B%E5%BA%8F%E5%91%98?icon=github&color=red"></a>
    <a href="https://mp.weixin.qq.com/s/xkZSp3606rTPN_JbLT3hSQ"> <img src="https://badgen.net/badge/follow/%E5%85%AC%E4%BC%97%E5%8F%B7?icon=rss&color=green"></a>
    <a href="https://space.bilibili.com/259649365"> <img src="https://badgen.net/badge/pick/B%E7%AB%99?icon=dependabot&color=blue"></a>
    <a href="https://mp.weixin.qq.com/s/wx-JkgOUoJhb-7ZESxl93w"> <img src="https://badgen.net/badge/join/%E4%BA%A4%E6%B5%81%E7%BE%A4?icon=atom&color=yellow"></a>
</div>




# Python自动化办公社区，团队协作仓库。

本仓库是 ⛳[Python自动化办公社区](https://gitee.com/zhaofeng092/python_auto_office/blob/master/%E5%85%B3%E9%94%AE%E8%AF%8D/%E7%BE%A4%E8%81%8A/%E5%85%A8%E7%BD%91%E5%90%8C%E5%90%8D.md) 发起维护的 团队 资源列表，内容包括：文章、视频配套代码，团队故事，网站开发、小程序开发等。由「[Python图书馆](https://space.bilibili.com/259649365)」、「[Web图书馆](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=Mzg3MDU3OTgxMg==&action=getalbum&album_id=1904783288632721409&scene=173&from_msgid=2247489811&from_itemidx=2&count=3&nolastread=1#wechat_redirect)」、「[栗姐说写作变现](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzI4MzE2Mzk1NA==&action=getalbum&album_id=1851500109679673345&scene=173&from_msgid=2649303563&from_itemidx=2&count=3&nolastread=1#wechat_redirect)」、「[法学万轻舟](https://mp.weixin.qq.com/mp/appmsgalbum?__biz=MzIxNzEzNTA4OA==&action=getalbum&album_id=2056406806263824384&scene=173&from_msgid=2650208172&from_itemidx=1&count=3&nolastread=1#wechat_redirect)」微信公号团队维护更新。


### 本项目的参与者

- 维护者：
  - [@看门大叔](https://gitee.com/kanmendashu)
  - [@凌晨四点](https://gitee.com/zjx15156495352)
  - [@山月呀](https://gitee.com/shanyueya)
  - [@万粥粥](https://gitee.com/wan-congee)
- 贡献者：
  - 


注：名单不分排名，不定期补充更新

## 作品列表


### Web图书馆

> 作者：@山月呀

*   
*   

------

### 团队故事

> 作者：@万粥粥

*   
*   



------

### Python图书馆

> 作者：@凌晨四点

*   
*   

------


### Python自动化办公社区

> 作者：@KanMenDaShu

*   [Python自动化办公（可能是B站内容最全的！有源代码 ，适合小白~）](https://www.bilibili.com/video/BV1y54y1i78U)
*   [Python自动化办公--Pandas玩转Excel（全30集）](https://www.bilibili.com/video/BV1hk4y1C73S)
*   [三大金融工具特训班——Wind、Excel、Python(完结)](https://www.bilibili.com/video/BV16U4y1g7mS)

------
