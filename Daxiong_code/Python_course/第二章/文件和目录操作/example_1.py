#列出指定目录下的所有py文件

import os

path=os.getcwd()    #获取当前工作目录

lst=os.listdir(path)    #列出指定目录下的所有文件和子目录

for filename in lst:
    if filename.endswith('.py'):
        print(filename)