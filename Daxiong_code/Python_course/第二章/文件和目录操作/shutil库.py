#1.复制   shutil.copyfile
#2.移动文件     shutil.move
#3.删除文件和文件夹     shutil.rmtree
#4.压缩与解压文件      shutil.make_archive     shutil.unpack_archive


import shutil

#copyfile仅仅是将文件的内容复制到另一个文件
#shutil.copyfile(来源文件,目标文件路径)
# shutil.copyfile(r"D:\系统盘存储\系统默认\桌面\Python之禅.txt",r"D:\系统盘存储\系统默认\桌面\test.txt")


#如果目标不是文件，而是文件夹。
# shutil.copy(文件来源，目标路径)
# shutil.copy(r"D:\系统盘存储\系统默认\桌面\Python之禅.txt",r'D:\系统盘存储\系统默认\桌面\第2章\黑丝yyds')


#也可以复制整个文件夹
# shutil.copytree(文件来源,目标路径)
# shutil.copytree(r'D:\系统盘存储\系统默认\桌面\第2章\批量更名',r'D:\系统盘存储\系统默认\桌面\第2章\11.11')

# 移动文件
# shutil.move(文件来源，目标路径)
# shutil.move(r"D:\系统盘存储\系统默认\桌面\第2章\批量更名\2020case2020.jpg",r'D:\系统盘存储\系统默认\桌面\第2章\黑丝yyds')

#删除文件夹和文件
# shutil.rmtree(目标路径)
# shutil.rmtree(r'D:\系统盘存储\系统默认\桌面\第2章\1')

#压缩与解压文件

#压缩
# shutil.make_archive(r'压缩包','zip',root_dir=r'D:\系统盘存储\系统默认\桌面\第2章')
# shutil.make_archive(r'D:\系统盘存储\系统默认\桌面\第2章\压缩包666','zip',root_dir=r'D:\系统盘存储\系统默认\桌面\第2章')


#解压
# shutil.unpack_archive(r"D:\系统盘存储\系统默认\桌面\第2章\压缩包.zip",extract_dir=r'D:\系统盘存储\系统默认\桌面\第2章\帅气',format='zip')
shutil.unpack_archive(r"D:\系统盘存储\系统默认\桌面\第2章\压缩包666.zip",extract_dir=r'D:\系统盘存储\系统默认\桌面\第2章\6666专用文件夹')
