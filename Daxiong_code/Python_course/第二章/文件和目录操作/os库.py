#写在前面：os模块是Python内置的操作系统功能和文件系统功能相关的模块，该模块通常和操作系统有关。

# 最大的亮点：直接调用系统文件

# import os

# os.system('notepad.exe')      #记事本

# os.system('calc.exe')         #计算器

# os.startfile(r"C:\Users\Public\Desktop\网易云音乐.lnk")      #调用可执行文件









# os常用的操作函数

# import os

# print(os.getcwd())      #获取当前工作目录

# print(os.listdir())     #打开当前目录下的文件夹

# print(os.listdir(r'F:\视频'))     #案例展示，本电脑F盘下面视频目录下面的文件

# os.mkdir('demo 10.py')      #创建文件夹

# os.makedirs('A/B/C')        #创建多级文件夹

# os.rmdir('demo 10.py')      #删除目录

# os.removedirs('A/B/C')      #删除多级目录







#os.path常用的函数

import os.path

# print(os.path.abspath('os库'))     #获取文件或者目录的绝对路径

# print(os.path.exists('os库.py'),os.path.exists('os.path库.py'))     #用于判断文件目录是否存在，存在返回TRUE，否则返回FALSE

# print(os.path.join(r'E:\python','demo 15.py'))      #把多个路径组合起来，以字符串第一个路径开始拼接
#
# print(os.path.split(r'E:\pycharm\学Python不加班\第二章\文件和目录操作\os库.py'))      #将文件名分割成目录和文件名
#
# print(os.path.splitext('os库.py'))      #分离后缀名（扩展名）
#
# print(os.path.basename(r'E:\pycharm\学Python不加班\第二章\文件和目录操作\os库.py'))        #从一个目录中提取文件名
#
# print(os.path.dirname(r'E:\pycharm\学Python不加班\第二章\文件和目录操作\os库.py'))        #提取出这个文件路径，不包括文件名


