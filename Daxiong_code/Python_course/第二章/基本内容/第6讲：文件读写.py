#open函数
# open('文件的位置','操作模式：r,w','编码模式，默认是UTF-8')
# r-read
# w-write


# 读取文件
# f=open(r"D:\系统盘存储\系统默认\桌面\英语精美的句子.txt",'r')   #r忽略一些转义

# text=f.read()
# text=f.readline()     #假设文件过大
# print(text)
# #
# #for循环
# f.seek(0)         #seek移动文件到指定的位置
# for line in f:
#     print(line,end='')
#

# 写入文本文件
# f=open(r"D:\系统盘存储\系统默认\桌面\test.txt",'w')
# f.write('只要你好好学习\n你就是个好人！')
# f.close()

