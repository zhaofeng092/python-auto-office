# 类的定义

# 举个梨子
# 每个人都是不一样的，都有不同的性格、爱好，不同的行为特征（比如吃饭、说话……）
# 我们定义一个Person类

# class Person:       #写一个Person类
#     def __init__(self):   #一个初始变量
#         self.name='大雄'    #输入初始的一个名字
#         self.age=21       #对应的年龄
#         self.like='拳击'    #对应的爱好
#
#     def say(self):        #定义一个say的函数
#         print('我是{},我今年{},我喜欢{}'.format(self.name,self.age,self.like))        #打印format语法，执行say函数的程序
# print("---------------")
# # 实例化
# p1=Person()  #取一个变量p1
# print(p1.name,p1.age,p1.like)   #打印出p1对应的姓名、年龄、爱好
# p1.say()
#
# print("---------------")
# p2=Person() #取一个变量p2
# p2.name='Tom'
# p2.age=18
# p2.like='游泳'
# print(p2.name,p2.age,p2.like)   #打印出p2对应的姓名、年龄、爱好
# p2.say()
#
# print("---------------")
# p3=Person() #取一个变量p3
# p3.name='Jack'
# p3.age=25
# p3.like='跑步'
# print(p3.name,p3.age,p3.like)   #打印出p3对应的姓名、年龄、爱好
# p3.say()

#类的三个特征
# 封装：打包函数和数据库。
# 继承：和现实生活中一样，如果不好好努力，那你只能回家继承几个亿的家产了，在Python中，子辈继承父辈的相关数据。
# 多态，很多数据，共用一个类，通过调用这个类，然后给他们重新写方法。

# 继承
class Person:
    def __init__(self):
        self.name = "人类"
        self.age = 21

    def say(self):
        print("我是{}，我今年{}岁".format(self.name, self.age))

    def sing(self):
        print("人类会唱歌")


class Programmer(Person):   #这里Programmer就是直接引用了上面Person的类
    def __init__(self):
        super().__init__()
        self.name = "大雄"

    def sing(self):
        print("程序员会唱歌！")


class Teacher(Person):
    def __init__(self):
        super().__init__()
        self.name = "小美"

    def sing(self):
        print("老师会唱歌！")

    def read(self):
        print("老师会阅读！")


class Student(Teacher, Programmer):
    def study(self):
        print("学生会学习！")


# 实例化
person = Person()
programmer = Programmer()
teacher = Teacher()
student = Student()
#
# #
# # # 继承的呈现
# print("programmer:", programmer.name, programmer.age)
# programmer.say()
# #
# #
# # # 多态
def do_something(o):   #定义一个函数“做某件事”
    o.sing()    #会查看sing这个方法


do_something(person)
do_something(programmer)
do_something(teacher)
do_something(student)
