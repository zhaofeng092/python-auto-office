# 数据类型与变量

#1. 数字

int#整数  1,2,3,4,5,6

float#浮点型  3.14

bool#布尔型  Ture/False False等值于0，Ture等值于1

# print(6+5)
# print(6-5)
# print(6*5)
# print(6**5)
# print(6/5)
# print(6//5)
# print(6%5)

# 2.字符串
# 'hello,world!'
# a=("hello, world!")
# print(type(a))
#
print('''He said :"It's love!"''')#单引号、双引号、三引号

# #字符串的基本操作

# print(1+2)
#
# print('1'+'2')


# 3.列表

# list_1=['a',1,1.1,'b']
# print(type(list_1))
#
# #查看列表元素个数
# print(len(list_1))
#
# #访问列表元素
# print(list_1[2])
# print(list_1[-1])
#
# #利用python的内置函数来创建列表
# list_2=list((100,1,1.5,'a'))
# print(list_2)

# 4.元组

# 元组可以看成特殊的列表
# tuple_1=(100,1.1,1,'a')
# print(type(tuple_1))
#
# # 利用python内置函数创建元组
# tuple_2=tuple((1,1.1,'a',100))
# print(type(tuple_2))
#
# #利用len函数统计元组中的元素个数
# print(len(tuple_1))
#
# #利用python的内置函数list，将元祖作为参数代入，返回列表对象
# list_1=list(tuple_1)
# print(list_1)

# 5.字典
# 字典中每个成员是以“键：值”对的形式存在的，字典中的键都是不重复的
# dict_1={'春':'spring','夏':'sunmmer','秋':'autumn','冬':'winter'}
# print(type(dict_1))
#
# #快速查找数据
# print(dict_1['夏'])

# 6.集合

#集合和字典类似，但是它仅包含键，没有值。由于键是不重复的，所以集合是不包含重复数据的数据集。

list_1=['春','夏','秋','冬','春','夏','秋','冬']
set_1=set(list_1)
print(set_1)
print(type(set_1))
#
# # #集合与列表的转换
list_1=list(set_1)#把集合的形式转换为列表
print(list_1)#打印该列表
print(type(list_1))#列表类型

print('各位小伙伴给个一键三连哦！')