#if语句
score=int(input('请输入你的分数：'))
if 90<=score<=100:
    print('A+')
elif 80<=score<90:
    print('A')
elif 70<=score<80:
    print('B')
elif 60<=score<70:
    print('C')
else:
    print('D')

#从1一直+到100的和

# j=1     #从1开始
# sum=0   #加法运算
# while j<101:   #开始while语句
#     sum+=j    #逐次递增
#     j+=1     #j的值逐次递增
# print(sum)   #1+到100的和
#
#
# sum=0     #从0开始
# for i in range(1,101):     #i是从1~100的数字
#     sum+=i     #sum每次加都是i
# print(sum)     #输出1加到100的和

# 游戏时间
# import random   #随机导入一个整数
#
# secret_num=random.randint(1,10)   #系统从这个函数包括1~10中取一个数字，
# print("请开始猜数字，游戏开始了哦！")
# while True:   #while循环语句
#     prompt="请输入一个幸运数字：\n"   #我们需要输入的数字
#     guess_input=input(prompt)   #系统猜的数字
#     your_guess=int(guess_input)   #将我们猜的数字转化为“整数”类型
#     if your_guess==secret_num:   #如果我们和系统的答案一致
#         print("你真是个小天才，恭喜您猜对了！")   #输出“恭喜您猜对了”的结果
#     elif your_guess>secret_num:   #如果我们的答案比系统的大
#         print("数值偏大，请重新猜一次")   #输出“大了”的提示
#     else:   #如果不是以上两种情况
#         print("数值偏小，请重新猜一次")   #输出“小了”的提示