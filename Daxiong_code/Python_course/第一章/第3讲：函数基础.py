# import random
#
# def play_game():
#     rand_num = random.randint(1, 10)    #在1~10之间的随机数
#     print(rand_num)                     #
#     guess_count = 0                     #我们猜的次数从第0次开始
#     totle_count = 3                     #一共有3次机会
#     while True:         #while循环
#         if guess_count == totle_count:   #当我们的答案与要猜的数字一致时
#             print("游戏接结束")            #游戏结束
#             break                        #退出
#         print("剩余次数：{}".format(totle_count - guess_count))   #猜的次数从3次递减
#         guess_num = int(input("请输入：\n"))            #输入我们要猜的数字
#         if guess_num > rand_num:
#             print("大了，请再来一次")
#         elif guess_num < rand_num:
#             print("小了，请再来一次")
#         else:
#             print("恭喜您对了！")
#             break
#         guess_count += 1
#
# #多人游戏，看在三次机会中，谁能够最先猜出答案
# play_game()
# play_game()
# play_game()



#一个简单的函数模型
# def print_str():   #定义一个函数
#     print('你真是个小天才')   #将定义的函数输出打印
#     # return '完毕'  #返回值（可省略）
#
# result=print_str()   #替换操作
# print(result)      #输出result



# 位置参数1
# def print_info():
#     print('我在看《百年孤独》')
#     return '这本书非常有意思。'   #返回值（可省略不写）
#
# result=print_info()   #替换操作
# print(result)     #输出结果

#
# #位置参数2
# def print_info(name,age):   #形参， 定义时的
#     print("%s 今年 %d" %(name,age))   #实参， 调用时的
#
# print_info("TOM",18)
#

# 关键字参数
def print_info2(name=None,age=0):
    print("%s 今年 %d" %(name,age))

print_info2(name='张三',age=18)

def print_info2(name=None,age=0,):
    print("%s 今年 %d" %(name,age))

print_info2(name='张三',age=18)
print_info2(name='李四',age=11)
print_info2(name='王五',age=35)


# 总结：
# 有时候，我们需要反复使用某段代码，可以将这段代码封装成函数，当我们需要时直接调用即可，函数的本质就是对功能的封装。
