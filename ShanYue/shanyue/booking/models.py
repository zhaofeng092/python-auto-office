from django.db import models

# Create your models here.
class Income(models.Model):
    Income_id = models.AutoField(primary_key=True)
    Income_date = models.DateField('时间',blank=True,null=True)
    Income_money = models.DecimalField('金额', max_digits=9, decimal_places=2, blank=False)
    Income_classify = models.CharField('分类',max_length=200,blank=True,null=True)
    Income_comments = models.CharField('备注',max_length=200,blank=True,null=True)
    Income_record = models.DateTimeField('填写时间',blank=True,null=True)



class Expend(models.Model):
    Expend_id = models.AutoField(primary_key=True)
    Expend_date = models.DateField('时间',blank=True,null=True)
    Expend_money = models.DecimalField('金额', max_digits=9, decimal_places=2, blank=False)
    Expend_classify = models.CharField('分类',max_length=200,blank=True,null=True)
    Expend_comments = models.CharField('备注',max_length=200,blank=True,null=True)
    Expend_record = models.DateTimeField('填写时间',blank=True,null=True)
