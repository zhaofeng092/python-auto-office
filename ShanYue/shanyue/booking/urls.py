from django.urls import path

from . import views

app_name = 'booking'
urlpatterns = [
    path('', views.index, name='index'),
    path('income/', views.income_input, name='income_input'),
    path('expend/', views.expend_input, name='expend_input'),
    path('income/results/', views.income_results, name='income_results'),
    path('expend/results/', views.expend_results, name='expend_results'),
]