from django.contrib import admin
from .models import Income, Expend


class IncomeAdmin(admin.ModelAdmin):
    list_display = ('Income_date', 'Income_money', 'Income_classify', 'Income_comments', 'Income_record')
    list_filter = ('Income_date', 'Income_classify')
    search_fields = ('Income_money','Income_classify__exact','Income_comments')
    date_hierarchy = 'Income_date'


class ExpendAdmin(admin.ModelAdmin):
    list_display = ('Expend_date', 'Expend_money', 'Expend_classify', 'Expend_comments', 'Expend_record')
    list_filter = ('Expend_date', 'Expend_classify')
    search_fields = ('Expend_money', 'Expend_classify__exact', 'Expend_comments')
    date_hierarchy = 'Expend_date'


admin.site.register(Income,IncomeAdmin)
admin.site.register(Expend,ExpendAdmin)

admin.site.site_title ='个人记账本'
admin.site.site_header ='记账本'
admin.site.index_title='理财就是理生活'