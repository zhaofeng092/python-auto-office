from django.shortcuts import get_object_or_404, render
from django.utils import timezone
from .models import Income,Expend


def index(request):
    return render(request, 'booking/index.html', {})

def income_input(request):
    return render(request, 'booking/income_input.html')

def expend_input(request):
    return render(request, 'booking/expend_input.html', {})

def income_results(request):
    if request.method == 'POST':
        Income_date = request.POST.get('Income_date');
        Income_money = request.POST.get('Income_money');
        Income_classify = request.POST.get('Income_classify');
        Income_comments = request.POST.get('Income_comments');
        Income_record = timezone.now();
        income = Income(Income_date=Income_date, Income_money=Income_money,
                        Income_classify=Income_classify,Income_comments=Income_comments,
                        Income_record=Income_record);
        income.save();
        return render(request, 'booking/income_results.html', {})

def expend_results(request):
    if request.method == 'POST':
        Expend_date = request.POST.get('Expend_date');
        Expend_money = request.POST.get('Expend_money');
        Expend_classify = request.POST.get('Expend_classify');
        Expend_comments = request.POST.get('Expend_comments');
        Expend_record = timezone.now();
        expend = Expend(Expend_date=Expend_date, Expend_money=Expend_money,
                        Expend_classify=Expend_classify,Expend_comments=Expend_comments,
                        Expend_record=Expend_record);
        expend.save();
        return render(request, 'booking/expend_results.html', {})


