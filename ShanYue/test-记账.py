import openpyxl
import datetime
from tkinter import messagebox
from tkinter import *
from tkinter import ttk
import os
import time
import calendar

windows = Tk()
windows.title("个人记账本")
windows.geometry('1200x640+125+70')
windows.configure(bg='white')
windows.iconbitmap("H://logo_py//logo.ico")
windows.resizable(0,0)

class App_create_excel(object):
    def __init__(self):
        if os.path.exists('记账本.xlsx'):
            pass

        else:
            zb = openpyxl.Workbook()
            sheet1 = zb.active
            sheet1.title = '收入'
            sheet1['A1'] = '日期'
            sheet1['B1'] = '金额'
            sheet1['C1'] = '类别'
            sheet1['D1'] = '备注'
            sheet1['E1'] = '填写日期'
            sheet1.column_dimensions['D'].width = 20
            sheet1.column_dimensions['E'].width = 20
            sheet1.row_dimensions[1].height = 20

            # 复制sheet1
            for ws in zb.worksheets:
                zb.copy_worksheet(from_worksheet=ws)

            sheet2 = zb.worksheets[1]
            sheet2.title = '支出'

            sheet3 = zb.create_sheet('资产', 2)
            sheet3.merge_cells('A1:L1')
            sheet3['A1'] = '资产'
            sheet3.merge_cells('M1:S1')
            sheet3['M1'] = '负债'

            sheet3.merge_cells('A2:C2')
            sheet3['A2'] = '日常'
            sheet3.merge_cells('D2:G2')
            sheet3['D2'] = '投资'
            sheet3.merge_cells('H2:J2')
            sheet3['H2'] = '银行卡'
            sheet3.merge_cells('K2:L2')
            sheet3['K2'] = '其它'
            sheet3.merge_cells('M2:O2')
            sheet3['M2'] = '日用性'
            sheet3.merge_cells('P2:Q2')
            sheet3['P2'] = '资产性'
            sheet3.merge_cells('R2:S2')
            sheet3['R2'] = '其它'

            sheet3['A3'] = '余额宝'
            sheet3['B3'] = '微信'
            sheet3['C3'] = '现金'
            sheet3['D3'] = '支付宝理财'
            sheet3['E3'] = '支付宝基金'
            sheet3['F3'] = '证券1'
            sheet3['G3'] = '证券2'
            sheet3['H3'] = '银行卡1'
            sheet3['I3'] = '银行卡2'
            sheet3['J3'] = '银行卡3'
            sheet3['K3'] = '借款'
            sheet3['L3'] = '其它'
            sheet3['M3'] = '花呗'
            sheet3['N3'] = '白条'
            sheet3['O3'] = '信用卡'
            sheet3['P3'] = '房贷'
            sheet3['Q3'] = '车贷'
            sheet3['R3'] = '欠款'
            sheet3['S3'] = '其它'

            sheet4 = zb.create_sheet('额度', 3)
            sheet4['A1'] = '填写日期'
            sheet4['B1'] = '计划额度'
            sheet4['C1'] = '备注'
            sheet4.column_dimensions['A'].width = 20
            sheet4.column_dimensions['C'].width = 20

            zb.save('记账本.xlsx')

class App_main(object):
    def __init__(self):
        self.frame_title = LabelFrame(windows, bg='white', labelanchor="n")
        self.frame_title.place(relx=0.01, rely=0.01, relwidth=0.98, relheight=0.16)
        self.Label_name = Label(self.frame_title, text="记账本", bg='white', font=('华文新魏', 30))
        self.Label_name.place(relx=0.45, rely=0.08)
        self.Label_introduction = Label(self.frame_title, text="//别想一下造出大海，必须先由小河川开始//", bg='white', font=('黑体', 10))
        self.Label_introduction.place(relx=0.4, rely=0.68)

        self.label_time = Label(self.frame_title,text="", font=('微软雅黑', 12), bg='white')
        self.label_time.place(relx=0.82, rely=0.2)
        self.update_clock()

        today = datetime.datetime.now().weekday() + 1
        week_day_dict = {1: '星期一', 2: '星期二', 3: '星期三', 4: '星期四', 5: '星期五', 6: '星期六', 7: '星期日', }
        week_china = week_day_dict[today]
        self.label_week = Label(self.frame_title,text=week_china, font=('微软雅黑', 12), bg='white')
        self.label_week.place(relx=0.86, rely=0.6)


        self.frame_1 = LabelFrame(windows, text="//记录每一笔收入与支出//", bg='white', font=('黑体', 15), labelanchor="n")
        self.frame_1.place(relx=0.01, rely=0.24, relwidth=0.485, relheight=0.18)
        self.button_in = Button(self.frame_1, text='收入记录', font=('微软雅黑', 12),command=App_收入, width=8, fg='white', bg='green')
        self.button_in.place(relx=0.18, rely=0.25)
        self.button_out = Button(self.frame_1, text='支出记录', font=('微软雅黑', 12), command=App_支出 , width=8, fg='white', bg='red')
        self.button_out.place(relx=0.68, rely=0.25)

        self.frame_2 = LabelFrame(windows, text="//查询收入or支出//", bg='white', font=('黑体', 15), labelanchor="n")
        self.frame_2.place(relx=0.505, rely=0.24, relwidth=0.485, relheight=0.18)
        self.button_date = Button(self.frame_2, text='通过日期查询',command=App_by_date, font=('微软雅黑', 12), width=12, fg='white', bg='green')
        self.button_date.place(relx=0.16, rely=0.3)
        self.button_class = Button(self.frame_2, text='通过类别查询',command=App_by_class, font=('微软雅黑', 12), width=12, fg='white', bg='red')
        self.button_class.place(relx=0.6, rely=0.3)


        self.frame_3 = LabelFrame(windows, text="//查看剩余可用额度//", bg='white', font=('黑体', 15), labelanchor="n")
        self.frame_3.place(relx=0.01, rely=0.48, relwidth=0.485, relheight=0.48)

        self.label_2_1 = Label(self.frame_3, text='请输入每年可用额度:', font=('微软雅黑', 10), bg='white')
        self.label_2_1.place(relx=0.12, rely=0.09)

        self.Entry_2_1 = Entry(self.frame_3, font=('微软雅黑', 10), width=21, bg='white')
        self.Entry_2_1.place(relx=0.36, rely=0.1)
        self.button_plan = Button(self.frame_3, text='确认',command=self.per_edu, font=('微软雅黑', 10), width=8, fg='white', bg='green')
        self.button_plan.place(relx=0.72, rely=0.08)

        self.label_2 = Label(self.frame_3, text='今年已过了:', font=('微软雅黑', 10), bg='white')
        self.label_2.place(relx=0.12, rely=0.3)
        self.per_date()

        self.label_3 = Label(self.frame_3, text='已花费:', font=('微软雅黑', 10), bg='white')
        self.label_3.place(relx=0.12, rely=0.5)
        self.self_output_3 = Text(self.frame_3, font=('微软雅黑', 10), bg='white', width=21, height=1)
        self.self_output_3.place(relx=0.36, rely=0.5)
        self.spend_excel()

        self.label_4 = Label(self.frame_3, text='用了额度的:', font=('微软雅黑', 10), bg='white')
        self.label_4.place(relx=0.12, rely=0.72)


        self.frame_4 = LabelFrame(windows, text="//统计现有资产//", bg='white', font=('黑体', 15), labelanchor="n")
        self.frame_4.place(relx=0.505, rely=0.48, relwidth=0.485, relheight=0.48)
        self.button_capital = Button(self.frame_4, text='点击查看', font=('微软雅黑', 12), width=8, fg='white', bg='green')
        self.button_capital.place(relx=0.42, rely=0.3)


    def update_clock(self):
        now_time = time.strftime("%Y-%m-%d %H:%M:%S")
        self.label_time.configure(text=now_time)
        self.frame_title.after(1000, self.update_clock)

    def per_date(self):
        self.canvas_time = Canvas(self.frame_3, width=168, height=30, bg="white")
        self.canvas_time.place(relx=0.36, rely=0.28)
        x = StringVar()

        self.out_per = self.canvas_time.create_rectangle(5, 5, 105, 25, outline="red", width=1)
        self.fill_per = self.canvas_time.create_rectangle(5, 5, 5, 25, outline="", width=0, fill="red")

        self.Label_per_1 = Label(self.frame_3, textvariable=x, font=('微软雅黑', 10), bg='white')
        self.Label_per_1.place(relx=0.56, rely=0.29)

        year_days = 366 if calendar.isleap(int(str('2021'))) else 365
        now_year = datetime.datetime.now().year
        now_month = datetime.datetime.now().month
        now_day = datetime.datetime.now().day
        time1 = datetime.datetime(now_year, now_month, now_day)
        time2 = datetime.datetime(now_year, 1, 1)
        over_days = (time1 - time2).days

        self.canvas_time.coords(self.fill_per, (5, 5, 6 + (over_days / year_days) * 100, 25))
        self.frame_3.update()
        x.set(str(round(over_days / year_days * 100, 2)) + '%')

    def per_edu(self):
        self.canvas_money = Canvas(self.frame_3, width=168, height=30, bg="white")
        self.canvas_money.place(relx=0.36, rely=0.7)
        x = StringVar()

        self.out_per = self.canvas_money.create_rectangle(5, 5, 105, 25, outline="red", width=1)
        self.fill_per = self.canvas_money.create_rectangle(5, 5, 5, 25, outline="", width=0, fill="red")

        self.Label_per_2 = Label(self.frame_3, textvariable=x, font=('微软雅黑', 10), bg='white')
        self.Label_per_2.place(relx=0.56, rely=0.71)

        self.money_plan = int(self.Entry_2_1.get().rstrip())
        self.money_spend = float(self.all_out_money)
        self.canvas_money.coords(self.fill_per, (5, 5, 6 + (self.money_spend / self.money_plan) * 100, 25))
        self.frame_3.update()
        x.set(str(round(self.money_spend / self.money_plan * 100, 2)) + '%')

    def spend_excel(self):
        filepath = '记账本.xlsx'
        zb = openpyxl.load_workbook(filepath)
        sheet1 = zb.worksheets[1]
        data = list(sheet1.values)
        item_list = [item for item in data][1:]
        out_list_money = []
        for item in item_list:
            zc_money = item[1]
            out_list_money.append(float(zc_money))

        self.all_out_money = sum(out_list_money)
        self.self_output_3.insert("insert",self.all_out_money)


    def run(self):
        windows.mainloop()


class App_收入(object):

    def __init__(self):
        self.top_收入 = Toplevel()

        self.top_收入.title('收入记录')
        self.top_收入.geometry('400x300+300+300')
        self.top_收入.configure(bg='white')

        self.label1 = Label(self.top_收入, text='日期:', font=('微软雅黑', 10), bg='white')
        self.label1.place(relx=0.2, rely=0.05)
        self.label2 = Label(self.top_收入, text='金额:', font=('微软雅黑', 10), bg='white')
        self.label2.place(relx=0.2, rely=0.2)
        self.label3 = Label(self.top_收入, text='类别:', font=('微软雅黑', 10), bg='white')
        self.label3.place(relx=0.2, rely=0.35)
        self.label4 = Label(self.top_收入, text='备注:', font=('微软雅黑', 10), bg='white')
        self.label4.place(relx=0.2, rely=0.5)

        self.Entry1 = Entry(self.top_收入, font=('微软雅黑', 9), width=20, bg='white')
        self.Entry1.place(relx=0.32, rely=0.05)

        self.Entry2 = Entry(self.top_收入, font=('微软雅黑', 9), width=20, bg='white')
        self.Entry2.place(relx=0.32, rely=0.2)

        self.xx_分类 = ttk.Combobox(self.top_收入, width=18)
        self.xx_分类.place(relx=0.32, rely=0.35)
        self.xx_分类['value'] = ('工资', '兼职', '理财', '人情', '其它')
        self.xx_分类.bind("<<ComboboxSelected>>", self.show)

        self.Entry4 = Entry(self.top_收入, font=('微软雅黑', 9), width=21, bg='white')
        self.Entry4.place(relx=0.32, rely=0.5)

        self.Button1 = Button(self.top_收入, text='写入数据', font=('微软雅黑', 9), width=8,
                              command=lambda: [f() for f in [self.jl_收入数据, self.top_收入.destroy]], fg='white',
                              bg='green')
        self.Button1.place(relx=0.3, rely=0.68)
        self.Button2 = Button(self.top_收入, text='退出', font=('微软雅黑', 9), width=6, command=self.top_收入.destroy,
                              fg='white', bg='green')
        self.Button2.place(relx=0.6, rely=0.68)

    def show(self, event):
        self.con_类别 = self.xx_分类.get()
        return self.con_类别

    def jl_收入数据(self):
        self.con_日期 = self.Entry1.get()
        self.con_日期 = self.con_日期.strip()
        self.con_金额 = self.Entry2.get()
        self.con_金额 = self.con_金额.strip()
        self.con_备注 = self.Entry4.get()
        self.con_备注 = self.con_备注.strip()

        if self.con_金额 == '':
            messagebox.showinfo('提示', message='请输入金额')

        else:
            filepath = '记账本.xlsx'
            zb = openpyxl.load_workbook(filepath)
            sheet1 = zb.worksheets[0]
            con_填写日期 = datetime.datetime.now()
            sheet1.append([self.con_日期, self.con_金额, self.con_类别, self.con_备注, con_填写日期])
            zb.save('记账本.xlsx')
            messagebox.showinfo('提示', message='收入数据已记录完成')



class App_支出(object):
    def __init__(self):
        self.top_支出 = Toplevel()

        self.top_支出.title('支出记录')
        self.top_支出.geometry('400x300+300+300')
        self.top_支出.configure(bg='white')

        self.label1 = Label(self.top_支出, text='日期:', font=('微软雅黑', 10), bg='white')
        self.label1.place(relx=0.2, rely=0.05)
        self.label2 = Label(self.top_支出, text='金额:', font=('微软雅黑', 10), bg='white')
        self.label2.place(relx=0.2, rely=0.2)
        self.label3 = Label(self.top_支出, text='类别:', font=('微软雅黑', 10), bg='white')
        self.label3.place(relx=0.2, rely=0.35)
        self.label4 = Label(self.top_支出, text='备注:', font=('微软雅黑', 10), bg='white')
        self.label4.place(relx=0.2, rely=0.5)

        self.Entry1 = Entry(self.top_支出, font=('微软雅黑', 9), width=20, bg='white')
        self.Entry1.place(relx=0.32, rely=0.05)

        self.Entry2 = Entry(self.top_支出, font=('微软雅黑', 9), width=20, bg='white')
        self.Entry2.place(relx=0.32, rely=0.2)

        self.xx_分类 = ttk.Combobox(self.top_支出, width=18)
        self.xx_分类.place(relx=0.32, rely=0.35)
        self.xx_分类['value'] = ('餐饮', '娱乐', '保险', '学习', '生活用品', '日常开销', '人情', '其它')
        self.xx_分类.bind("<<ComboboxSelected>>", self.show)

        self.Entry4 = Entry(self.top_支出, font=('微软雅黑', 9), width=21, bg='white')
        self.Entry4.place(relx=0.32, rely=0.5)

        self.Button1 = Button(self.top_支出, text='写入数据', font=('微软雅黑', 9), width=8,
                              command=lambda: [f() for f in [self.jl_支出数据, self.top_支出.destroy]], fg='white',
                              bg='green')
        self.Button1.place(relx=0.3, rely=0.68)
        self.Button2 = Button(self.top_支出, text='退出', font=('微软雅黑', 9), width=6, command=self.top_支出.destroy,
                              fg='white', bg='green')
        self.Button2.place(relx=0.6, rely=0.68)

    def show(self, event):
        self.con_类别 = self.xx_分类.get()
        return self.con_类别

    def jl_支出数据(self):
        self.con_日期 = self.Entry1.get()
        self.con_日期 = self.con_日期.strip()
        self.con_金额 = self.Entry2.get()
        self.con_金额 = self.con_金额.strip()
        self.con_备注 = self.Entry4.get()
        self.con_备注 = self.con_备注.strip()

        if self.con_金额 == '':
            messagebox.showinfo('提示', message='请输入金额')

        else:
            filepath = '记账本.xlsx'
            zb = openpyxl.load_workbook(filepath)
            sheet1 = zb.worksheets[1]
            con_填写日期 = datetime.datetime.now()
            sheet1.append([self.con_日期, self.con_金额, self.con_类别, self.con_备注, con_填写日期])

            zb.save('记账本.xlsx')
            messagebox.showinfo('提示', message='支出数据已记录完成')




class App_by_date(object):

    def __init__(self):
        self.top_by_date = Toplevel()

        self.top_by_date.title('通过日期查询')
        self.top_by_date.geometry('400x400+100+100')
        self.top_by_date.configure(bg='white')

        self.frame_out = LabelFrame(self.top_by_date, text="//支出查询//", bg='white', font=('黑体', 15), labelanchor="n")
        self.frame_out.place(relx=0.01, rely=0.01, relwidth=0.98, relheight=0.47)

        self.label_out_1 = Label(self.frame_out, text='请输入你要查询的日期:', font=('微软雅黑', 10), bg='white')
        self.label_out_1.place(relx=0.08, rely=0.05)
        self.label_out_2 = Label(self.frame_out, text='年:', font=('微软雅黑', 10), bg='white')
        self.label_out_2.place(relx=0.18, rely=0.24)
        self.label_out_3 = Label(self.frame_out, text='月:', font=('微软雅黑', 10), bg='white')
        self.label_out_3.place(relx=0.18, rely=0.44)
        self.label_out_4 = Label(self.frame_out, text='日:', font=('微软雅黑', 10), bg='white')
        self.label_out_4.place(relx=0.18, rely=0.64)
        self.Entry_out_1 = Entry(self.frame_out, font=('微软雅黑', 10), width=12, bg='white')
        self.Entry_out_1.place(relx=0.32, rely=0.25)
        self.Entry_out_2 = Entry(self.frame_out, font=('微软雅黑', 10), width=12, bg='white')
        self.Entry_out_2.place(relx=0.32, rely=0.45)
        self.Entry_out_3 = Entry(self.frame_out, font=('微软雅黑', 10), width=12, bg='white')
        self.Entry_out_3.place(relx=0.32, rely=0.65)

        self.button_sure_out = Button(self.frame_out, text='确定', command=App_date_out,font=('微软雅黑', 10), width=8, fg='white', bg='green')
        self.button_sure_out.place(relx=0.7, rely=0.4)


        self.frame_in = LabelFrame(self.top_by_date, text="//收入查询//", bg='white', font=('黑体', 15), labelanchor="n")
        self.frame_in.place(relx=0.01, rely=0.51, relwidth=0.98, relheight=0.47)

        self.label_in_1 = Label(self.frame_in, text='请输入你要查询的日期:', font=('微软雅黑', 10), bg='white')
        self.label_in_1.place(relx=0.08, rely=0.05)
        self.label_in_2 = Label(self.frame_in, text='年:', font=('微软雅黑', 10), bg='white')
        self.label_in_2.place(relx=0.18, rely=0.24)
        self.label_in_3 = Label(self.frame_in, text='月:', font=('微软雅黑', 10), bg='white')
        self.label_in_3.place(relx=0.18, rely=0.44)
        self.label_in_4 = Label(self.frame_in, text='日:', font=('微软雅黑', 10), bg='white')
        self.label_in_4.place(relx=0.18, rely=0.64)
        self.Entry_in_1 = Entry(self.frame_in, font=('微软雅黑', 10), width=12, bg='white')
        self.Entry_in_1.place(relx=0.32, rely=0.25)
        self.Entry_in_2 = Entry(self.frame_in, font=('微软雅黑', 10), width=12, bg='white')
        self.Entry_in_2.place(relx=0.32, rely=0.45)
        self.Entry_in_3 = Entry(self.frame_in, font=('微软雅黑', 10), width=12, bg='white')
        self.Entry_in_3.place(relx=0.32, rely=0.65)

        self.button_sure_in = Button(self.frame_in, text='确定', command=App_date_in, font=('微软雅黑', 10), width=8, fg='white', bg='green')
        self.button_sure_in.place(relx=0.7, rely=0.4)

class App_by_class(object):

    def __init__(self):
        self.top_by_class = Toplevel()

        self.top_by_class.title('通过类别查询')
        self.top_by_class.geometry('400x400+100+100')
        self.top_by_class.configure(bg='white')

        self.frame_out = LabelFrame(self.top_by_class, text="//支出查询//", bg='white', font=('黑体', 15), labelanchor="n")
        self.frame_out.place(relx=0.01, rely=0.01, relwidth=0.98, relheight=0.47)

        self.label_out_1 = Label(self.frame_out, text='请选择你要查询的类别:', font=('微软雅黑', 10), bg='white')
        self.label_out_1.place(relx=0.1, rely=0.12)

        self.out_分类 = ttk.Combobox(self.frame_out, width=18)
        self.out_分类.place(relx=0.3, rely=0.36)
        self.out_分类['value'] = ('餐饮', '娱乐', '保险', '学习', '生活用品', '日常开销', '人情', '其它')
        self.out_分类.bind("<<ComboboxSelected>>", self.show_out)

        self.out_results = Button(self.frame_out, text='点击查看结果', command=App_class_out, font=('微软雅黑', 10), width=10, fg='white', bg='green')
        self.out_results.place(relx=0.36, rely=0.62)


        self.frame_in = LabelFrame(self.top_by_class, text="//收入查询//", bg='white', font=('黑体', 15), labelanchor="n")
        self.frame_in.place(relx=0.01, rely=0.51, relwidth=0.98, relheight=0.47)

        self.label_in_1 = Label(self.frame_in, text='请选择你要查询的类别:', font=('微软雅黑', 10), bg='white')
        self.label_in_1.place(relx=0.1, rely=0.12)

        self.in_分类 = ttk.Combobox(self.frame_in, width=18)
        self.in_分类.place(relx=0.3, rely=0.36)
        self.in_分类['value'] = ('工资', '兼职', '理财', '人情', '其它')
        self.in_分类.bind("<<ComboboxSelected>>", self.show_in)

        self.in_results = Button(self.frame_in, text='点击查看结果', command=App_class_in, font=('微软雅黑', 10), width=10, fg='white', bg='green')
        self.in_results.place(relx=0.36, rely=0.62)


    def show_out(self, event):
        self.select_类别 = self.in_分类.get()
        return self.select_类别

    def show_in(self, event):
        self.select_类别 = self.in_分类.get()
        return self.select_类别


class App_date_out(object):
    def __init__(self):
        self.date_out = Toplevel()
        self.date_out.title('支出的日期查询')  # 设置标题
        self.date_out.geometry("500x400+600+100")

        tree = ttk.Treeview(self.date_out, columns=["日期", "金额", "分类", "备注"])
        tree.place(x=0.1, y=0.1)

        # 设置列属性，列不显示
        tree.column("#0", width=60, anchor="center")  # 首列的标识为“#0”，可以用column来进行设置。(后面的依次为#1，#2...)
        tree.column('日期', width=100, anchor='center')
        tree.column('金额', width=60, anchor='center')
        tree.column('分类', width=60, anchor='center')
        tree.column('备注', width=150, anchor='center')

        # 设置表头
        tree.heading("日期", text="日期")
        tree.heading("金额", text="金额")
        tree.heading("分类", text="分类")
        tree.heading("备注", text="备注")

        # 添加数据
        li1 = ["2021-11-13", "1625.2", "其它", "没有备注"]
        li2 = ["2021-11-13", "18.2", "其它", "你想想看有什么备注"]

        # 添加数据
        tree.insert('', 'end', text="1", values=li1)  # 添加数据到末尾
        tree.insert('', 'end', text="2", values=li2)

class App_date_in(object):
    def __init__(self):
        self.date_in = Toplevel()
        self.date_in.title('收入的日期查询')  # 设置标题
        self.date_in.geometry("500x400+600+100")

        tree = ttk.Treeview(self.date_in, columns=["日期", "金额", "分类", "备注"])
        tree.place(x=0.1, y=0.1)

        # 设置列属性，列不显示
        tree.column("#0", width=60, anchor="center")  # 首列的标识为“#0”，可以用column来进行设置。(后面的依次为#1，#2...)
        tree.column('日期', width=100, anchor='center')
        tree.column('金额', width=60, anchor='center')
        tree.column('分类', width=60, anchor='center')
        tree.column('备注', width=150, anchor='center')

        # 设置表头
        tree.heading("日期", text="日期")
        tree.heading("金额", text="金额")
        tree.heading("分类", text="分类")
        tree.heading("备注", text="备注")

        # 添加数据
        li1 = ["2021-11-13", "1625.2", "其它", "没有备注"]
        li2 = ["2021-11-13", "18.2", "其它", "你想想看有什么备注"]

        # 添加数据
        tree.insert('', 'end', text="1", values=li1)  # 添加数据到末尾
        tree.insert('', 'end', text="2", values=li2)

class App_class_out(object):
    def __init__(self):
        self.class_out = Toplevel()
        self.class_out.title('支出的分类查询')  # 设置标题
        self.class_out.geometry("500x400+600+100")

        tree = ttk.Treeview(self.class_out, columns=["日期", "金额", "分类", "备注"])
        tree.place(x=0.1, y=0.1)

        # 设置列属性，列不显示
        tree.column("#0", width=60, anchor="center")  # 首列的标识为“#0”，可以用column来进行设置。(后面的依次为#1，#2...)
        tree.column('日期', width=100, anchor='center')
        tree.column('金额', width=60, anchor='center')
        tree.column('分类', width=60, anchor='center')
        tree.column('备注', width=150, anchor='center')

        # 设置表头
        tree.heading("日期", text="日期")
        tree.heading("金额", text="金额")
        tree.heading("分类", text="分类")
        tree.heading("备注", text="备注")

        # 添加数据
        li1 = ["2021-11-13", "1625.2", "其它", "没有备注"]
        li2 = ["2021-11-13", "18.2", "其它", "你想想看有什么备注"]

        # 添加数据
        tree.insert('', 'end', text="1", values=li1)  # 添加数据到末尾
        tree.insert('', 'end', text="2", values=li2)

class App_class_in(object):
    def __init__(self):
        self.class_in = Toplevel()
        self.class_in.title('收入的分类查询')  # 设置标题
        self.class_in.geometry("500x400+600+100")

        tree = ttk.Treeview(self.class_in, columns=["日期", "金额", "分类", "备注"])
        tree.place(x=0.1, y=0.1)

        # 设置列属性，列不显示
        tree.column("#0", width=60, anchor="center")  # 首列的标识为“#0”，可以用column来进行设置。(后面的依次为#1，#2...)
        tree.column('日期', width=100, anchor='center')
        tree.column('金额', width=60, anchor='center')
        tree.column('分类', width=60, anchor='center')
        tree.column('备注', width=150, anchor='center')

        # 设置表头
        tree.heading("日期", text="日期")
        tree.heading("金额", text="金额")
        tree.heading("分类", text="分类")
        tree.heading("备注", text="备注")

        # 添加数据
        li1 = ["2021-11-13", "1625.2", "其它", "没有备注"]
        li2 = ["2021-11-13", "18.2", "其它", "你想想看有什么备注"]

        # 添加数据
        tree.insert('', 'end', text="1", values=li1)  # 添加数据到末尾
        tree.insert('', 'end', text="2", values=li2)



if __name__=="__main__":
    app=App_create_excel()
    app = App_main()

    app.run()
