<div align="center">
    <a href="https://github.com/zhaofeng092/python_auto_office"> <img src="https://badgen.net/badge/Github/%E7%A8%8B%E5%BA%8F%E5%91%98?icon=github&color=red"></a>
    <a href="https://mp.weixin.qq.com/s/xkZSp3606rTPN_JbLT3hSQ"> <img src="https://badgen.net/badge/follow/%E5%85%AC%E4%BC%97%E5%8F%B7?icon=rss&color=green"></a>
    <a href="https://space.bilibili.com/259649365"> <img src="https://badgen.net/badge/pick/B%E7%AB%99?icon=dependabot&color=blue"></a>
    <a href="https://mp.weixin.qq.com/s/wx-JkgOUoJhb-7ZESxl93w"> <img src="https://badgen.net/badge/join/%E4%BA%A4%E6%B5%81%E7%BE%A4?icon=atom&color=yellow"></a>
</div>
1、配置团队协作环境

- 前置工作：
  - 注册[gitee](https://gitee.com/)，必须用邮箱注册
  - 下载安装[PyCharm](https://www.jetbrains.com/pycharm/)，免费的**Community**版本即可
  - 下载安装[Git](https://git-scm.com/)
  - 下载安装[Typora](https://www.typora.io/)
  - 在PyCharm里，配置Git和gitee账户👉[教程](https://www.cnblogs.com/hanmk/p/13546223.html) （本教程，只需要完成前3步）
- 在群里@大雄\@兆锋，等待远程培训
